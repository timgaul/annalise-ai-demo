import AWS from 'aws-sdk';
import { injectable } from 'tsyringe';

@injectable()
export class ImageRepository {

    private static BUCKET_NAME = "dev-annalise-ai-demo-bucket";

    public uploadObject = async (buf: Buffer, imageName: string, contentType: string) => {
        const data = {
            Bucket: ImageRepository.BUCKET_NAME,  // TODO get this from a configuration
            Key: imageName,
            Body: buf,
            ContentType: contentType
        };

        const s3 = new AWS.S3();
        return s3.upload(data).promise()
    }
}