
import "reflect-metadata";
import AWS from "aws-sdk";
import { container } from 'tsyringe';
import { TagRepository } from '../TagRepository';
import { mockDynamoDBInstance } from '../../mocks/AWSMock';

jest.mock('aws-sdk', () => ({
    DynamoDB: jest.fn(() => mockDynamoDBInstance)
}))

describe('Happy paths', () => {
    let sut: TagRepository;

    beforeEach(() => {
        // mock out dyanamodb calls
        mockDynamoDBInstance.query.mockReturnValue({
            promise: () => Promise.resolve([{ a: 1 }, { b: 2 }]) as any
        });
        sut = container.resolve(TagRepository);
    })

    test("Puts an item in DynamoDB", async () => {
        // arrange
        const imageId = 'fake-image-id';
        const tag = 'image-tag';

        const params = {
            TableName: 'dev-dynamodb-table',
            Item: {
                partitionKey: { S: imageId },
                sortKey: { S: tag }
            }
        }

        // act
        await sut.putItem(imageId, tag);

        // assert
        expect(mockDynamoDBInstance.putItem).toBeCalledWith(params)
    });

    test("Get an item from DynamoDB", async () => {
        // arrange
        const imageId = 'fake-image-id';

        // act
        const result = await sut.getItems(imageId);

        // assert
        expect(result).toMatchObject([{ a: 1 }, { b: 2 }])
    });
})
