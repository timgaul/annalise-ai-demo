import AWS from 'aws-sdk';
import { injectable } from 'tsyringe';

@injectable()
export class TagRepository {

    private static TABLE_NAME = "dev-dynamodb-table"; // TODO: config faile
    private dynamodb = new AWS.DynamoDB();
    
    public putItem = async (imageId: string, tag: string) => {
        const params = {
            TableName: TagRepository.TABLE_NAME,
            Item: {
                partitionKey: { S: imageId },
                sortKey: { S: tag }            
            }
        }
        return this.dynamodb.putItem(
            params
        ).promise();
    }

    public getItems = async (imageId: string) => {
        const params = {
            TableName: TagRepository.TABLE_NAME,
            KeyConditionExpression: "partitionKey = :i",
            ExpressionAttributeValues: {
                [':i']: { 'S': imageId }
            }    
        }
        return this.dynamodb.query(params).promise();   
    }
}