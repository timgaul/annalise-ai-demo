export const mockDynamoDBInstance = {
    putItem: jest.fn(() => {
        return {
            promise: () => Promise.resolve()
        }
    }),
    query: jest.fn(() => {
        return {
            promise: () => Promise.resolve()
        }
    })
}
