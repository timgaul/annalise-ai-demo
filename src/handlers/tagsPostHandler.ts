import 'reflect-metadata';
import { Context, APIGatewayProxyResult, APIGatewayEvent } from 'aws-lambda';
import { container } from 'tsyringe';
import winston from 'winston';
import WinstonCloudWatch from 'winston-cloudwatch';
import { TagService } from "../services/TagService";

export const handler = async (event: APIGatewayEvent, context: Context): Promise<APIGatewayProxyResult> => {
    winston.add(new WinstonCloudWatch({
        logGroupName: 'tagsGet',
        logStreamName: event.requestContext.requestId
    }));

    winston.info('event', { event })
    winston.info('UserId', { userId: event.requestContext.identity })

    // TODO: check for JSON
    try {
         // TODO middy
        if (!event.body) {
            return {
                statusCode: 400,
                body: 'Bad Request: no body.'
            }        
        }
        const { imageId, tag } = JSON.parse(event.body);
        if (!imageId) {
            return {
                statusCode: 400,
                body: 'Bad Request: imageId is required.'
            }
        } 
        if (!tag) {
            return {
                statusCode: 400,
                body: 'Bad Request: tag is required.'
            }
        } 
        const tagService = container.resolve(TagService);
        // TODO: validate image is in bucket
        await tagService.addImageTag(imageId, tag);
        return {
            statusCode: 200,
            body: "OK",
        };
    } catch (e) {
        winston.error('Unexpected error', { e })
        return {
            statusCode: 500,
            body: JSON.stringify(e)
        }
    }
};