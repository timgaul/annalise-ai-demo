import 'reflect-metadata';
import { Context, APIGatewayProxyResult, APIGatewayEvent } from 'aws-lambda';
import { container } from "tsyringe";
import winston from 'winston';
import WinstonCloudWatch from 'winston-cloudwatch';
import { TagService } from '../services/TagService';


export const handler = async (event: APIGatewayEvent, context: Context): Promise<APIGatewayProxyResult> => {
    winston.add(new WinstonCloudWatch({
        logGroupName: 'tagsGet',
        logStreamName: event.requestContext.requestId
    }));

    winston.info('event', { event })
    winston.info('UserId', { userId: event.requestContext.identity })

    const { imageId } = event.pathParameters ?? {};
    const tagService = container.resolve(TagService);
    // TODO middy
    try {
        if (imageId) {
            const result = await tagService.getImageTags(imageId);
            return {
                statusCode: 200,
                body: JSON.stringify(result)
            };
        } else {
            return {
                statusCode: 400,
                body: 'Bad Request: imageId is required.'
            }
        }
    } catch (e) {
        winston.error('Unexpected error', { e })
        return {
            statusCode: 500,
            body: JSON.stringify(e)
        }
    }
};