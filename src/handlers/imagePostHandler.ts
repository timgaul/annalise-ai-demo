import "reflect-metadata";
import { Context, APIGatewayProxyResult, APIGatewayEvent } from 'aws-lambda';
import winston from 'winston';
import WinstonCloudWatch from 'winston-cloudwatch';
import AWS from "aws-sdk";
import { container } from "tsyringe";
import { ImageService } from "services/ImageService";

export const handler = async (event: APIGatewayEvent, context: Context): Promise<APIGatewayProxyResult> => {
    winston.add(new WinstonCloudWatch({
        logGroupName: 'tagsGet',
        logStreamName: event.requestContext.requestId,
        awsRegion: 'ap-southeast-2'
    }));

    winston.info('event', { event })
    winston.info('UserId', { userId: event.requestContext.identity })

    container.register('winston', { useValue: winston });

    // TODO check for JSON
    try {
        // TODO middy
        if (!event.body) {
            return {
                statusCode: 400,
                body: 'Bad Request: no body'
            }
        }
        if (!event.headers) {
            return {
                statusCode: 400,
                body: 'Bad Request: Content-Type header is required'
            }
        }

        const contentType = event.headers!['Content-Type'] ?? event.headers!['content-type'];
        winston.info('Uploading content type', { contentType, headers: event.headers })

        if (!contentType || !['image/jpeg', 'image/png'].includes(contentType)) {
            return {
                statusCode: 400,
                body: 'Bad Request: Image must be for type .jpg or .png'
            }
        }

        const imageService = container.resolve(ImageService);
        const buf = Buffer.from(event.body.replace(/^data:image\/\w+;base64,/, ""), "base64");
        const result = await imageService.uploadImage(buf, contentType);

        return {
            statusCode: 200,
            body: JSON.stringify({
                fileName: result.Location
            }),
        };
    } catch (e) {
        winston.error('Error uploading image', { error: e });
        return {
            statusCode: 500,
            body: JSON.stringify(e)
        }
    }
};