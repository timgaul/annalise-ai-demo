import { injectable, inject } from "tsyringe"
import { TagRepository } from "../repository/TagRepository"

@injectable()
export class TagService {
    constructor(
        @inject(TagRepository) private tagRepository: TagRepository
    ) {
        //
    }

    public addImageTag = async (imageId: string, tag: string): Promise<void> => {        
        await this.tagRepository.putItem(imageId, tag)
    }

    public getImageTags = async (imageId: string): Promise<any[]> => { // TODO    
        const { Items } = await this.tagRepository.getItems(imageId);
        return Items ?? [];
    }
}