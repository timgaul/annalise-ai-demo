import { ImageRepository } from "repository/ImageRepository";
import { inject, injectable } from "tsyringe";
import { v4 as uuidv4 } from 'uuid';

@injectable()
export class ImageService {
    constructor(
        @inject(ImageRepository) private imageRepository: ImageRepository,
        @inject('winston') private winston: any // TODO
    ) {
        //
    }

    public uploadImage = async (buf: Buffer, contentType: string) => {
        const imageName = `${uuidv4()}`;

        this.winston.info('Saving image to S3 bucket', { buf, imageName, contentType })

        return this.imageRepository.uploadObject(buf, imageName, contentType);
    }
}