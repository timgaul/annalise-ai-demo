# annalise-ai-demo

Demo project for annalise.ai. This project mimics an AWS environment

## Getting started

Install dependencies
```
yarn
```

### AWS deployment
<p>

Set you AWS keys
```
export AWS_ACCESS_KEY_ID=AKIA3EPH2GCHEMQ***** 
export AWS_SECRET_ACCESS_KEY=sP/DFdBXQwmq3OPl7n0FxSw7Y+JiGsyjQ5g*****
```

Deploy to AWS
```
yarn deploy --config serverless.dev.ts
```

## Documentation

### Technologies used

- Inversion of Control, using TSyringe
- Cloud storage
    - Amazon S3
- API Gateway
    - Authentication Lambda
- AWS Lambdas for logic
- NoSQL database
    - DynamoDB
- Binary uploads
- Serverless Framework for deployment
- Winston for logging

### Diagram
<br>

![image info](./assets/annalise-demo-diagram.png)

## Requirements

&check; have an API written in TypeScript or Python<br/>
&check; have endpoints for images and tags<br/>
&check; persist data and/or metadata to a database<br/> 
&check; be secure against anonymous access<br/>
&check; contain unit tests<br/>
&check; track which user/client has interacted with the API<br/>
&cross; support searching for tagged images by date<br/>

## What is missing?

- CORS
- ANY method
- Batch updates
- Checking image actually exists
- DELETE images
- DELETE tags
- GET images for a particular tag
- Serialization middleware
- Schema validation middleware
- Unit tests on all files
- Integration or e2e testing
- Authentication via secure provider, such as Cognito or Auth0
- CI/CD
- Canary deployments of Lambdas, using versioning, to limit down time